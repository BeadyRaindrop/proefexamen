package domain;

import java.io.Serializable;

public class Spy implements Serializable {

    private String codeName;
    private String realName;
    private int licence;


    public Spy(String codeName, String realName, int license) {
        this.codeName = codeName;
        this.realName = realName;
        this.licence = license;
    }


    public String getCodeName() {
        return codeName;
    }

    public String getRealName() {
        return realName;
    }

    public int getLicence() {
        return licence;
    }

    @Override
    public String toString() {
        return "Spy " + codeName + " AKA " + realName + " has " + licence + " licences";
    }
}
