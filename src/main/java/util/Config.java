package util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

public class Config {

    private static final String CONFIG_FILE = "/config/config.properties";
    private static final Config INSTANCE = new Config();

    private final Properties properties = new Properties();

    private Config(){

       try(InputStream ris = getClass().getResourceAsStream(CONFIG_FILE)){
           properties.load(ris);
       } catch (IOException e) {
           e.printStackTrace();
       }
    }

    public static Config getInstance(){
        return INSTANCE;
    }

    public String readSetting(String key){
        return properties.getProperty(key);
    }

}
