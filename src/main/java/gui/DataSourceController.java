package gui;

import data.*;
import domain.Spy;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import util.Config;
import util.Crypto;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DataSourceController {

    SpiesRepository inMemory = new InMemorySpiesRepo();
    SpiesRepository sql = new MySQLSpiesRepo();
    SpiesRepository plain = new PlainRepo();
    SpiesRepository encrypted = new EncryptedRepo();



    @FXML
    private Label label;

    @FXML
    private ComboBox<SpiesRepository> comboBox;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button button;

    @FXML
    private Label errors;


    @FXML
    void initialize(){  //ADDING OPTIONS HERE --> COMBOBOX
//        List<SpiesRepository> dataSources = new ArrayList<>();
//
//        dataSources.add(inMemory);
//        dataSources.add(sql);
//        dataSources.add(plain);
//        dataSources.add(encrypted);
//
//        ObservableList<SpiesRepository> observableList = FXCollections.observableList(dataSources);
//        comboBox.setItems(observableList);
        comboBox.getItems().addAll(Repositories.ENCRYPTED_REPO, Repositories.IN_MEMORY_REPO, Repositories.PLAIN_REPO, Repositories.SQL_REPO);
    }

    public void logIn(ActionEvent actionEvent) throws IOException {
        SpiesRepository selectedDataSource = comboBox.getValue();
        String pwd = passwordField.getText();
        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/spies.fxml"));
        Parent p = loader.load();
        SpyController spyController = loader.getController();
        spyController.setDataSource(comboBox.getSelectionModel().getSelectedItem());
        Scene scene = new Scene(p);
        stage.setScene(scene);


        if (selectedDataSource.equals(inMemory)){
            stage.showAndWait();
        }
        else if (selectedDataSource.equals(plain)){
            stage.showAndWait();
        } else {
            if (checkPwd(pwd)){
                stage.showAndWait();
            }
            else {
                errors.setText("Incorrect Password");
            }
        }
    }

    private boolean checkPwd(String pwd){
        String dbPassword = Crypto.getInstance().decrypt( Config.getInstance().readSetting("spypwd") )   ;
        return dbPassword.equals(pwd);
    }
}