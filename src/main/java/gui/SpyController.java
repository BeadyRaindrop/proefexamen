package gui;

import data.Repositories;
import data.SpiesRepository;
import domain.Spy;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.List;

public class SpyController {

    private SpiesRepository spiesRepo;

    public void setDataSource(SpiesRepository selectedItem){
        this.spiesRepo = selectedItem;
        updateSpiesList();
    }

    @FXML
    public ListView list;

    @FXML
    public TextField codeName;

    @FXML
    public TextField realName;

    @FXML
    public CheckBox licence;

    @FXML
    private Button delete;

    @FXML
    private Button add;


    @FXML
    void addButton(ActionEvent event) {
        String codeNameText = codeName.getText();
        String realNameText = realName.getText();
        boolean isSelected = licence.isSelected();

        if (isSelected){
            Spy spyToAdd = new Spy(codeNameText, realNameText, 1);
            spiesRepo.addSpy(spyToAdd);
        } else {
            Spy spyToAdd = new Spy(codeNameText, realNameText, 0);
            spiesRepo.addSpy(spyToAdd);
        }
        updateSpiesList();
    }

    @FXML
    void updateSpiesList(){
        List<Spy> spiesList = spiesRepo.getSpies();
        ObservableList<Spy> observableList = FXCollections.observableList(spiesList);
        list.setItems(observableList);
    }

    public void delete(ActionEvent actionEvent) {
        Spy spy = (Spy) list.getSelectionModel().getSelectedItem();
        spiesRepo.delSpy(spy.getCodeName());
        updateSpiesList();
    }
}
