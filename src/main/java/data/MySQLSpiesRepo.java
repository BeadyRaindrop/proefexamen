package data;

import data.util.MySQLConnection;
import domain.Spy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MySQLSpiesRepo implements SpiesRepository {

    String SQL_GET_ALL_SPIES = "SELECT * FROM SPIES";
    String SQL_ADD_SPY = "INSERT INTO SPIES VALUES (?, ?, ?)";
    String SQL_DELETE_SPY = "DELETE FROM SPIES WHERE codeName = ?";


    private static final Logger LOGGER = Logger.getLogger(MySQLSpiesRepo.class.getName());


    @Override
    public List<Spy> getSpies() {

        List<Spy> spies = new ArrayList<>();

        try (Connection con = MySQLConnection.getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(SQL_GET_ALL_SPIES);
             ResultSet res = preparedStatement.executeQuery()) {

            while (res.next()){
                String codeName = res.getString("codeName");
                String realName = res.getString("realName");
                int licence = res.getInt("licence");

                Spy spyToAdd = new Spy(codeName, realName, licence);
                spies.add(spyToAdd);
            }
            return spies;

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            throw new IllegalArgumentException("rip lol");
        }
    }

    @Override
    public void addSpy(Spy spy) {

        try (Connection con = MySQLConnection.getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(SQL_ADD_SPY);
             )
        {
            preparedStatement.setString(1, spy.getCodeName());
            preparedStatement.setString(2, spy.getRealName());
            preparedStatement.setInt(3, spy.getLicence());

            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delSpy(String codename) {
        try(Connection con = MySQLConnection.getConnection();
            PreparedStatement stmt = con.prepareStatement(SQL_DELETE_SPY)){
            stmt.setString(1, codename);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
            throw new IllegalArgumentException("Unable to remove spy");
        }
    }


    @Override
    public String toString() {
        return "SQL";
    }
}
