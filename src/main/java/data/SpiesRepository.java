package data;

import domain.Spy;

import java.util.List;

public interface SpiesRepository {
    List<Spy> getSpies();
    void addSpy(Spy spy);
    void delSpy(String codename);
}
