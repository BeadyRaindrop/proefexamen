package data;

import domain.Spy;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlainRepo implements SpiesRepository {

    @Override
    public List<Spy> getSpies() {

       return Collections.emptyList();
    }

    @Override
    public void addSpy(Spy spy) {

    }

    @Override
    public void delSpy(String codename) {

    }

    @Override
    public String toString() {
        return "Plain";
    }
}
