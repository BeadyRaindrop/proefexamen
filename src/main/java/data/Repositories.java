package data;

public class Repositories {


    public static final SpiesRepository SQL_REPO = new MySQLSpiesRepo();
    public static final SpiesRepository IN_MEMORY_REPO = new InMemorySpiesRepo();
    public static final SpiesRepository PLAIN_REPO = new PlainRepo();
    public static final SpiesRepository ENCRYPTED_REPO = new EncryptedRepo();


    private Repositories(){
    }
}
