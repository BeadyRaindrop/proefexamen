package data;

import domain.Spy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InMemorySpiesRepo implements SpiesRepository {

    private List<Spy> spies;

    public InMemorySpiesRepo(){
        spies = new ArrayList<>();
    }


    @Override
    public List<Spy> getSpies() {
        return spies;
    }

    @Override
    public void addSpy(Spy spy) {
        spies.add(spy);
    }

    @Override
    public void delSpy(String codename) {
        spies.removeIf(spy -> spy.getCodeName().equals(codename));
    }



    @Override
    public String toString() {
        return "In memory";
    }
}
