package data;

import domain.Spy;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EncryptedRepo implements SpiesRepository {
    @Override
    public List<Spy> getSpies() {

        try (FileInputStream fis = new FileInputStream("./spies.txt");
             ObjectInputStream ois = new ObjectInputStream(fis)
        ){

            return (List<Spy>) ois.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Override
    public void addSpy(Spy spy) {

        List<Spy> spiesList = new ArrayList<>(getSpies());
        spiesList.add(spy);


        try (FileOutputStream fos = new FileOutputStream("./spies.txt");
             ObjectOutputStream out = new ObjectOutputStream(fos)
        ) {
            out.writeObject(spiesList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delSpy(String codename) {
        List<Spy> spiesList = new ArrayList<>(getSpies());
        spiesList.removeIf(spy -> spy.getCodeName().equals(codename));

        try (FileOutputStream fos = new FileOutputStream("./spies.txt");
             ObjectOutputStream out = new ObjectOutputStream(fos)
        ) {
            out.writeObject(spiesList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String toString() {
        return "Encrypted";
    }
}
